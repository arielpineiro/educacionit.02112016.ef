﻿using static System.Console;

namespace Delegados
{
    public class Program
    {
        private delegate int MyDelegate(int a, int b);

        private delegate void Print(string message);

        private static void Main(string[] args)
        {
            // Ejemplo 1
            int a = 2;
            int b = 3;
            MyDelegate f = Subs;
            int resultado = f(a, b);
            WriteLine(resultado);

            // Ejemplo 2
            string message = "Hello world!";
            Print print = WriteLine;
            print(message);
        }

        private static int Add(int a, int b)
        {
            return a + b;
        }

        private static int Subs(int a, int b)
        {
            return a - b;
        }
    }
}