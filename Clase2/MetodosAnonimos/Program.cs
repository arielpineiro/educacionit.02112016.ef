﻿using static System.Console;

namespace MetodosAnonimos
{
    public class Program
    {
        private delegate int MyDelegate(int a, int b);

        private delegate void Print(string message);

        private static void Main(string[] args)
        {
            // Ejemplo 1
            int a = 2;
            int b = 3;
            MyDelegate f = delegate (int x, int y)
            {
                return x + y;
            };
            int resultado = f(a, b);
            WriteLine(resultado);

            // Ejemplo 2
            Print print = delegate (string message)
            {
                WriteLine(message);
            };
            print("Hello world!");
        }
    }
}