﻿using System;

namespace ExpresionesLambda
{
    public class Program
    {
        private delegate int MyDelegate(int a, int b);

        private delegate void Print(string message);

        private static void Main(string[] args)
        {
            // Ejemplo 1
            int a = 2;
            int b = 3;

            MyDelegate f = (x, y) => { return x + y; };
            f = (x, y) => x + y;
            int resultado = f(a, b);
            Console.WriteLine(resultado);

            // Ejemplo 2
            // Template Lambda () => { // Code here ; } ;
            Print print1 = (message) => Console.WriteLine(message);
            Print print2 = (string message) =>
            {
                Console.WriteLine(message);
            };
        }

        private static void Something(Print print, string message)
        {
            print(message);
        }
    }
}