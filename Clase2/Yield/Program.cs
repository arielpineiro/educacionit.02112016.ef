﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yield
{
    class Program
    {
        static void Main(string[] args)
        {
            var words = GetWords();

            foreach (var word in words)
            {
                Console.WriteLine(word);
            }

            words.First(x => x.Equals("hola"));
        }

        static IEnumerable<string> GetWords()
        {
            yield return "hola";
            yield return "saludos";
            yield return "desde";
            yield return "argentina";
        }
    }
}
