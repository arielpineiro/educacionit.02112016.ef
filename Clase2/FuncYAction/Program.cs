﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace FuncYAction
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ejemplo 1
            int a = 2;
            int b = 3;

            Func<int, int, int> f = (x, y) => { return x + y; };
            f = (x, y) => x + y;
            int resultado = f(a, b);
            WriteLine(resultado);

            // Ejemplo 2
            // Template Lambda () => { // Code here ; } ;
            Action<string> print1 = (message) => WriteLine(message);
            Action<string> print2 = (string message) =>
            {
                WriteLine(message);
            };
            print1("Hola");
            print2("Mundo");
        }
    }
}
