﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            MyCollection<int> collection = new MyCollection<int>();
            collection.Add(1);
            collection.Add(3);
            collection.Add(2);
            collection.Add(9);            

            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }

            int first = collection.First();
            Console.WriteLine("Primer elemento: " + first);

            int last = collection.Last();
            Console.WriteLine("Ultimo elemento: " + last);

            MyCollection<int> result = collection.From(2);
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Func<int, bool> f = (x) => x > 5;
            Console.WriteLine("Mayores a 5...");
            MyCollection<int> result2 = collection.Search(x => x > 5);
            foreach (var item in result2)
            {
                Console.WriteLine(item);
            }

            List<string> cadenas = new List<string> { "Hello", "World!" };
            cadenas.Where(x => x.Contains("a"));

            Console.WriteLine("Multiplicar a todos los valores por 2. ");
            foreach (var item in collection.Map(x => x * 2))
            {
                Console.WriteLine(item);
            }

            var result3 = collection
                .Where(x => x > 2)
                .Select(x => x)
                .First();

            // Proyeccion (select * from Object)
            var result4 = from n in collection
                          select n;

            // Proyeccion (select o.* from Object o where o > 2 )
            var result5 = from n in collection
                          where n > 2
                          select n;

            // Proyeccion (select (o * 2) from Object o where o > 2 )
            var result6 = from n in collection
                          where n > 2
                          select n * 2;

            // Proyeccion con el primero (select (o * 2) from Object o where o > 2 )
            var result7 = (from n in collection
                          where n > 2
                          select n * 2).First();


        }
    }

    public class MyCollection<T> : IEnumerable<T>
    {
        Node<T> head = default(Node<T>);

        public void Add(T element)
        {
            Node<T> node = new Node<T>(element);

            if (head == null)
            {
                head = node;
            }
            else
            {
                Node<T> current = head;
                while (current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = node;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            Node<T> current = head;

            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    public class Node<T>
    {
        T value;
        Node<T> next;

        public Node(T value)
        {
            this.value = value;
        }

        public Node<T> Next
        {
            get { return next; }
            set { next = value; }
        }

        public T Value
        {
            get { return value; }
        }
    }
}
