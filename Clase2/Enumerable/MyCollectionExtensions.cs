﻿
using System;

namespace Enumerable
{
    public static class MyCollectionExtensions
    {
        public static T First<T>(this MyCollection<T> elements)
        {
            foreach (T element in elements)
                return element;

            return default(T);
        }

        public static T Last<T>(this MyCollection<T> elements)
        {
            T result = default(T);

            foreach (T element in elements)
            {
                result = element;
            }

            return result;
        }

        public static MyCollection<T> From<T>(this MyCollection<T> elements, int from)
        {
            MyCollection<T> result = new MyCollection<T>();

            int current = 0;
            foreach (T element in elements)
            {
                if (current++ < from)
                    continue;
                result.Add(element);
            }

            return result;
        }

        public static MyCollection<T> Search<T>(this MyCollection<T> elements, Func<T, bool> predicate)
        {
            MyCollection<T> result = new MyCollection<T>();

            foreach (T element in elements)
            {
                if (predicate(element))
                    result.Add(element);
            }

            return result;
        }

        public static MyCollection<T> Map<T>(this MyCollection<T> elements, Func<T, T> predicate)
        {
            MyCollection<T> result = new MyCollection<T>();

            foreach (T element in elements)
            {
                T output = predicate(element);
                result.Add(output);
            }

            return result;
        }
    }
}
