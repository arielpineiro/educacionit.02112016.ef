﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyUniversity
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new UniversityContext())
            {
                Student student = new Student
                {
                    FirstName = "Juan",
                    LastName = "Perez",
                    Mount = 4000.0
                };

                //db.People.Add(student);
                //db.SaveChanges();

                foreach (var item in db.People.OfType<Student>())
                {
                    Console.WriteLine(item.FirstName);
                    Console.WriteLine(item.LastName);
                }

                DbParameter parameter = new SqlParameter("@Id", 1);
                var result = db
                    .Database
                    .SqlQuery<Student>("GetStudents @Id", parameter);

                Console.WriteLine("Resultado SP: ");
                foreach (var item in result)
                {
                    Console.WriteLine(item.FirstName);
                    Console.WriteLine(item.LastName);
                }
            }            
        }
    }

    [Table("People")]
    abstract class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    [Table("Students")]
    class Student : Person
    {
        public double Mount { get; set; }
    }

    [Table("Instructors")]
    class Instructor : Person
    {
        public double Salary { get; set; }
        public ICollection<Course> Courses { get; set; }
    }

    class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    class UniversityContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Person> People { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Instructor>().ToTable("Instructors");
            modelBuilder.Entity<Student>().ToTable("Students");
        }
    }
}
