﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroEF
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindDb db = new NorthwindDb();

            var customers = db.Customers
                .Where(x => x.FirstName.Contains("a"))
                .OrderBy(x => x.Id)
                .Select(x => x);

            foreach (var item in customers)
            {
                Console.WriteLine(item.FirstName);
            }

            Customer customer = new Customer
            {
                FirstName = "Jorge",
                LastName = "Sanchez"
            };

            db.Customers.Add(customer);
            db.SaveChanges();
        }
    }
}
