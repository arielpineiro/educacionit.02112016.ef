﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonimos
{
    class Program
    {
        static void Main(string[] args)
        {
            // Tipos anonimos        
            var person = new 
            {
                Id = 1,
                FirstName = "Jorge",
                LastName = "Sanchez",
                ZipCode = 1234                
            };

            Console.WriteLine(person.FirstName);
            Console.WriteLine(person.GetType().Name);
        }
    }
}
