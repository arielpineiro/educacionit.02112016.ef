﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class StringHelper
    { 
        
        public static string Invertir(this string cadena)
        {
            char[] buffer = cadena.ToCharArray();
            Array.Reverse(buffer);
            return new string(buffer);
        }
    }
}
