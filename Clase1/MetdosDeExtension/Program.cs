﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetdosDeExtension
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Input: "hola";
            // Output: "aloh";

            string input = "hola";
            string output = input.Invertir();
            Console.WriteLine(output);
        }
    }
}
