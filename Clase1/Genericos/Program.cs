﻿using System;
using System.Collections;
using System.Collections.Generic;
    
namespace Genericos
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Coleccion Generica
            //List<string> values = new List<string>();
            //values.Add("Jorge");
            //values.Add("Juan");
            //values.Add("Maria");
            //values.Add("Cecilia");
            //values.Add("Daniel");

            //bool exist = CheckIfNameExist(values, "Maria");
            //if (exist)
            //    Console.WriteLine("Existe el nombre");
            #endregion            

            MyValue<string> value = new MyValue<string>("Hello world!");
            value.Show();

            string a = "hola";
            string b = "mundo";

            Swap(ref a, ref b);    
        }

        static void Swap<T>(ref T input, ref T output)
        {
            T aux = input;
            input = output;
            output = aux;
        }

        private static bool CheckIfNameExist(List<string> values, string name)
        {
            foreach (var item in values)
            {
                if (item == name)
                    return true;
            }
            return false;
        }
    }

    class MyValue<T>
    {
        T value;

        public MyValue(T value)
        {
            this.value = value;
        }

        public void Show()
        {
            Console.WriteLine(value.GetType().Name);
        }
    }
}
