﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inicializadores
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person1 = new Person();
            person1.Id = 1;
            person1.FirstName = "Jorge";
            person1.LastName = "Sanchez";
            person1.ZipCode = 1234;

            // Inicializador de Objeto        
            Person person2 = new Person()
            {
                Id = 1,
                FirstName = "Jorge",
                LastName = "Sanchez",
                ZipCode = 1234
            };

            // Inicializador de Coleccion
            List<int> values = new List<int> { 1, 2, 44, 5, 7, 9 };            
        }
    }

    class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ZipCode { get; set; }
    }
}
