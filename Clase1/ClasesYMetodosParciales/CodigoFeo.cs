﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasesYMetodosParciales
{
    partial class Codigo
    {
        partial void Inicia()
        {
            Console.WriteLine("Inicia...");
        }

        partial void Termina()
        {
            Console.WriteLine("Termina...");
        }

        public void Operacion()
        {
            Inicia();
            Console.WriteLine("Procesamiento...");
            Termina();
        }
    }
}
