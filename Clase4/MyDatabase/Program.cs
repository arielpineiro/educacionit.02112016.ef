﻿using MyDatabase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            using (BlogContext db = new BlogContext())
            {
                Blog blog = db.Blogs.First();

                Post post = new Post
                {
                    Title = "C# 6 Release Note",
                    Content = "Now support for null conditional operators",
                    Blog = blog
                };

                db.Posts.Add(post);

                db.SaveChanges();

            }                        
        }
    }
}
