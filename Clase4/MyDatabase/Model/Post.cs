﻿using System.ComponentModel.DataAnnotations;

namespace MyDatabase.Model
{
    public class Post
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Title { get; set; }

        [MaxLength(1024)]
        public string Content { get; set; }

        public Blog Blog { get; set; }
    }
}