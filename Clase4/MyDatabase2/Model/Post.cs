namespace MyDatabase2.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Post
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(1024)]
        public string Content { get; set; }

        public int? Blog_Id { get; set; }

        public virtual Blog Blog { get; set; }
    }
}
